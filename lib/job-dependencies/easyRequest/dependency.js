/**

easyRequest

- Provides a layer of abstraction to easily query HTTP endpoints.
- It does all the error handling (bad response, authentication failed, bad json response, etc).
*/

module.exports = function (){
  var request = require('request');

  function queryRequest (options, callback) {
    request(options, function(error, response, body){
      var errorMsg = null;
      
      if (error || !response || response.statusCode != 200) {
        errorMsg = (error || (response ? ("bad statusCode: " + response.statusCode) : "bad response")) + " from " + options.url;
      }
      
      callback(errorMsg, body, response);
    });
  }
  
  return {

    /**
      Provides an abstraction over request to query HTTP endpoints
      expecting the response in JSON format.
    */
    JSON: function(options, callback){
      queryRequest(options, function (error, body, response){
        var jsonBody;
        
        try {
          jsonBody = JSON.parse(body);
        }
        catch (e) {
          if (error) {
            jsonBody = null;
          } else {
            error = 'Invalid json response';
            jsonBody = null;
          }
        }
        
        callback(error, jsonBody, response);
      });
    },

    /**
      Provides an abstraction over request to query HTTP endpoints
      expecting the response in plain text or HTML format.
    */
    HTML: function(options, callback){
      queryRequest(options, function (error, body, response) {
        callback(error, body, response);
      });
    }

  };
};